//
//  APIConstants.swift
//  20210622-MattMartindale-NYCSchools
//
//  Created by Matthew Martindale on 6/22/21.
//

import Foundation

struct APIConstants {
    static let baseSchoolsURLString = "https://data.cityofnewyork.us/resource/s3k6-pzi2.json"
    static let baseSATURLString     = "https://data.cityofnewyork.us/resource/f9bf-2cp4.json"
    static let tokenKeyParam        = "$$app_token"
    static let dbnKeyParam          = "dbn"
}
