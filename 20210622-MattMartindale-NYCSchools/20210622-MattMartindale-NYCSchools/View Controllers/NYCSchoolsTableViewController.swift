//
//  NYCSchoolsTableViewController.swift
//  20210622-MattMartindale-NYCSchools
//
//  Created by Matthew Martindale on 6/22/21.
//

import UIKit

class NYCSchoolsTableViewController: UITableViewController {
    
    // MARK: - Properties
    let apiController = APIController()
    let spinnerView = SpinnerViewController()
    var schools: [School]? {
        // When fetchSchools completionHandler completes and updates schools property, reload tableView on Main thread
        didSet {
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
        }
    }

    // MARK: - Lifecycles
    override func viewDidLoad() {
        super.viewDidLoad()
        createSpinner()
        fetchSchools()
    }
    
    // MARK: - Methods
    func fetchSchools() {
        apiController.fetchSchools { [weak self] result in
            guard let self = self else { return }
            switch result {
            case .success(let schools):
                self.schools = schools
                DispatchQueue.main.async {
                    self.spinnerView.willMove(toParent: nil)
                    self.spinnerView.view.removeFromSuperview()
                    self.spinnerView.removeFromParent()
                }
            case .failure(let error):
                switch error {
                case .apiError(let error):
                    print("API error: \(error)")
                case .badResponse(let response):
                    print("invalid response: \(response)")
                case .decodeError(let error):
                    print("Decoding error: \(error)")
                case .invalidData:
                    print("Invalid data")
                case .noResponse:
                    print("No response")
                case .badQuery:
                    print("Bad search query")
                }
            }
        }
    }
    
    func createSpinner() {
        addChild(spinnerView)
        spinnerView.view.frame = view.frame
        view.addSubview(spinnerView.view)
        spinnerView.didMove(toParent: self)
    }

    // MARK: - Table view data source
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.schools?.count ?? 0
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SchoolCell", for: indexPath)
        
        let school = self.schools?[indexPath.row]
        cell.textLabel?.text = school?.schoolName

        return cell
    }
    
    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "SchoolDetailSegue" {
            if let schoolDetailVC = segue.destination as? SchoolDetailViewController {
                if let indexPath = tableView.indexPathForSelectedRow {
                    schoolDetailVC.school = self.schools?[indexPath.row]
                }
                schoolDetailVC.apiController = apiController
            }
        }
    }

}
