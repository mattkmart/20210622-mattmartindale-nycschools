//
//  NetworkTests.swift
//  20210622-MattMartindale-NYCSchoolsTests
//
//  Created by Matthew Martindale on 6/22/21.
//

import XCTest
@testable import _0210622_MattMartindale_NYCSchools

class NetworkTests: XCTestCase {
    
    func testValidResponse() {
        let mockAPIController = MockAPIController(data: validData, response: validResponse, error: nil)
        
        fetch(urlSessionCaller: mockAPIController) { result in
            switch result {
            case .success(let schools):
                XCTAssertNotNil(schools)
                XCTAssertNotEqual(schools.count, 0)
                XCTAssertEqual(schools.first?.dbn, "02504")
                XCTAssertEqual(schools.first?.schoolName, "Chester A. Arthur")
            case .failure(let error):
                print("Failed: \(error)")
                XCTFail()
            }
        }
    }
    
    func testValidData() {
        let mockAPIController = MockAPIController(data: validData, response: validResponse, error: nil)
        
        fetch(urlSessionCaller: mockAPIController) { result in
            switch result {
            case .success(let schools):
                XCTAssertNotNil(schools)
                XCTAssertNotEqual(schools.count, 0)
                XCTAssertEqual(schools[1].dbn, "3k43k")
                XCTAssertEqual(schools[1].schoolName, "Richmond Hill High School")
            case .failure(let error):
                print("Failed: \(error)")
                XCTFail()
            }
        }
    }
    
    func testInvalidData() {
        let mockAPIController = MockAPIController(data: invalidData, response: validResponse, error: nil)
        
        fetch(urlSessionCaller: mockAPIController) { result in
            switch result {
            case .success(let schools):
                XCTFail("Passed back valid data. School name: \(schools.first?.schoolName), when should pass back an error")
            case .failure(let error):
                XCTAssertNotNil(error)
            }
        }
    }
    
    func testInvalidResponse() {
        let mockAPIController = MockAPIController(data: validData, response: invalidResponse, error: nil)
        
        fetch(urlSessionCaller: mockAPIController) { result in
            switch result {
            case .success(let schools):
                XCTFail("Passed back valid data AND valid reponse. School name: \(schools.first?.schoolName) when should pass back an invalid response")
            case .failure(let error):
                XCTAssertNotNil(error)
            }
        }
    }
    
    func testValidError() {
        let mockAPIController = MockAPIController(data: validData, response: validResponse, error: Validerror)
        
        fetch(urlSessionCaller: mockAPIController) { result in
            switch result {
            case .success(_):
                XCTFail("Passed back valid data AND valid reponse, when should pass back an error")
            case .failure(let error):
                XCTAssertNotNil(error)
            }
        }
    }
    
    var validData: Data {
        let schoolJSON = [School(dbn: "02504", schoolName: "Chester A. Arthur"),
                          School(dbn: "3k43k", schoolName: "Richmond Hill High School")]
        let jsonEncoder = JSONEncoder()
        return try! jsonEncoder.encode(schoolJSON)
    }
    
    var invalidData: Data {
        let badSchoolJSON = [
            "dbn":000,
            "school_name":000,
            "website":000,
            "phone_number":000,
            "total_students":000
        ]
        let jsonEncoder = JSONEncoder()
        return try! jsonEncoder.encode(badSchoolJSON)
    }
    
    var validResponse: URLResponse {
        var urlComponents = URLComponents(string: APIConstants.baseSchoolsURLString)
        let apiTokenQuery = URLQueryItem(name: APIConstants.tokenKeyParam, value: APIToken.apiToken)
        urlComponents?.queryItems = [apiTokenQuery]
        
        let requestURL = urlComponents?.url
        let goodResponse = HTTPURLResponse(url: requestURL!, statusCode: 200, httpVersion: nil, headerFields: nil)!
        
        return goodResponse
    }
    
    var invalidResponse: URLResponse {
        var urlComponents = URLComponents(string: APIConstants.baseSchoolsURLString)
        let apiTokenQuery = URLQueryItem(name: APIConstants.tokenKeyParam, value: APIToken.apiToken)
        urlComponents?.queryItems = [apiTokenQuery]
        
        let requestURL = urlComponents?.url
        let badResponse = HTTPURLResponse(url: requestURL!, statusCode: 404, httpVersion: nil, headerFields: nil)!
        
        return badResponse
    }
    
    var Validerror: Error {
        return NSError(domain: "com.mattmartindale.20210622-MattMartindale_NYCSchools", code: -1, userInfo: nil)
    }
    
    func fetch(urlSessionCaller: URLSessionCaller, completion: @escaping (Result<[School], APIError>) -> Void) {
        let apiExpectation = expectation(description: "Fetching...")
        let urlSessionCaller = APIController(urlSessionCaller: urlSessionCaller)
        urlSessionCaller.fetchSchools { result in
            switch result {
            case .success(let schools):
                completion(.success(schools))
            case .failure(let error):
                completion(.failure(error))
            }
            apiExpectation.fulfill()
        }
        waitForExpectations(timeout: 4) { _ in
        }
    }

}
