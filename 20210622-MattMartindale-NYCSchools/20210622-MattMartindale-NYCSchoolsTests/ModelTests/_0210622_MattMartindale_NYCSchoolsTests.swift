//
//  _0210622_MattMartindale_NYCSchoolsTests.swift
//  20210622-MattMartindale-NYCSchoolsTests
//
//  Created by Matthew Martindale on 6/22/21.
//

import XCTest
@testable import _0210622_MattMartindale_NYCSchools

class _0210622_MattMartindale_NYCSchoolsTests: XCTestCase {
    
    func testSchoolDecoder() throws {
        let jsonData = schoolsJSON.data(using: .utf8)!
        
        let jsonDecoder = JSONDecoder()
        let schoolResult = try jsonDecoder.decode([School].self, from: jsonData)
        
        XCTAssertNotNil(schoolResult)
        XCTAssertEqual(schoolResult.first?.schoolName, "Clinton School Writers & Artists, M.S. 260", "Incorrect school name")
        XCTAssertNotEqual(schoolResult.count, 0)
    }
    
    func testSATScoreDecoder() throws {
        let jsonData = satScoreJSON.data(using: .utf8)!
        
        let jsonDecoder = JSONDecoder()
        let satResult = try jsonDecoder.decode([SATScores].self, from: jsonData)
        
        XCTAssertNotNil(satResult)
        XCTAssertNotEqual(satResult.count, 0)
        XCTAssertEqual(satResult.first?.numberOfTestTakers, "29", "Incorrect number of test takers")
        XCTAssertEqual(satResult.first?.readingScore, "355", "SAT reading score should be 355")
        XCTAssertEqual(satResult.first?.mathScore, "404", "SAT math score should be 404")
        XCTAssertEqual(satResult.first?.writingScore, "363", "SAT writing score should be 363")
    }

}
