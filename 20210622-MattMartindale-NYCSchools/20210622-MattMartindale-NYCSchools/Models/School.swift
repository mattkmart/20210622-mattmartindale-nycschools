//
//  School.swift
//  20210622-MattMartindale-NYCSchools
//
//  Created by Matthew Martindale on 6/22/21.
//

import Foundation

struct School: Codable {
    let dbn: String
    let schoolName: String
    
    enum CodingKeys: String, CodingKey {
        case dbn
        case schoolName = "school_name"
    }
}
