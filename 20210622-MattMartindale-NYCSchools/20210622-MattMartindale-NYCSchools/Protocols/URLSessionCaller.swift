//
//  URLSessionCaller.swift
//  20210622-MattMartindale-NYCSchools
//
//  Created by Matthew Martindale on 6/22/21.
//

import Foundation

// Implement a custom protocol for URLSession that will allow testing of Network code
protocol URLSessionCaller {
    func fetch(with request: URLRequest, completion: @escaping (Data?, URLResponse?, Error?) -> Void)
}

// Extend URLSession to adopt custom protocol
extension URLSession: URLSessionCaller {
    func fetch(with request: URLRequest, completion: @escaping (Data?, URLResponse?, Error?) -> Void) {
        let task = dataTask(with: request, completionHandler: completion)
        task.resume()
    }
}
