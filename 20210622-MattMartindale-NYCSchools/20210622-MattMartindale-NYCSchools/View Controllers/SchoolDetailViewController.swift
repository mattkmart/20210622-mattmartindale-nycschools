//
//  SchoolDetailViewController.swift
//  20210622-MattMartindale-NYCSchools
//
//  Created by Matthew Martindale on 6/22/21.
//

import UIKit

class SchoolDetailViewController: UIViewController {
    
    // MARK: - IBOutlets
    @IBOutlet weak var schoolNameLabel: UILabel!
    @IBOutlet weak var numberOfTestTakersLabel: UILabel!
    @IBOutlet weak var readingScoreLabel: UILabel!
    @IBOutlet weak var mathScoreLabel: UILabel!
    @IBOutlet weak var writingScoreLabel: UILabel!
    
    // MARK: - Properties
    var school: School?
    var apiController: APIController?
    let spinnerView = SpinnerViewController()
    
    // MARK: - Lifecycles
    override func viewDidLoad() {
        super.viewDidLoad()
        createSpinner()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        fetchSATScores()
    }
    
    // MARK: - Methods
    func fetchSATScores() {
        if let school = school {
            apiController?.fetchSAT(for: school, completion: { [weak self] result in
                guard let self = self else { return }
                switch result {
                case .success(let scores):
                    DispatchQueue.main.async {
                        self.spinnerView.willMove(toParent: nil)
                        self.spinnerView.view.removeFromSuperview()
                        self.spinnerView.removeFromParent()
                        self.updateViews(from: scores)
                    }
                case .failure(let error):
                    switch error {
                    case .apiError(let error):
                        print("API error: \(error)")
                    case .badResponse(let response):
                        print("invalid response: \(response)")
                    case .decodeError(let error):
                        print("Decoding error: \(error)")
                    case .invalidData:
                        print("Invalid data")
                    case .noResponse:
                        print("No response")
                    case .badQuery:
                        print("Bad search query")
                    }
                }
            })
        }
    }
    
    func updateViews(from satScores: [SATScores]) {
        if let score = satScores.first,
           let school = school {
            schoolNameLabel.text = school.schoolName
            numberOfTestTakersLabel.text = score.numberOfTestTakers
            readingScoreLabel.text = score.readingScore
            mathScoreLabel.text = score.mathScore
            writingScoreLabel.text = score.writingScore
        } else {
            // Some schools do not have SAT scores provided, this informs the User when this happens in a non-intrusive way.
            schoolNameLabel.text = "No SAT scores available"
        }
    }
    
    func createSpinner() {
        addChild(spinnerView)
        spinnerView.view.frame = view.frame
        view.addSubview(spinnerView.view)
        spinnerView.didMove(toParent: self)
    }
    
}
