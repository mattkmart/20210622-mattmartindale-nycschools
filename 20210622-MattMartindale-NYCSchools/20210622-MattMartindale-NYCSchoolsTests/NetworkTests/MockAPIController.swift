//
//  MockAPIController.swift
//  20210622-MattMartindale-NYCSchoolsTests
//
//  Created by Matthew Martindale on 6/22/21.
//

import Foundation
@testable import _0210622_MattMartindale_NYCSchools

class MockAPIController: URLSessionCaller {
    
    let data: Data?
    let response: URLResponse?
    let error: Error?
    
    init(data: Data?, response: URLResponse?, error: Error?) {
        self.data = data
        self.response = response
        self.error = error
    }
    
    func fetch(with request: URLRequest, completion: @escaping (Data?, URLResponse?, Error?) -> Void) {
            completion(self.data, self.response, self.error)
    }
}
