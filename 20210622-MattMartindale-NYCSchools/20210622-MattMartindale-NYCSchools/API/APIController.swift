//
//  APIController.swift
//  20210622-MattMartindale-NYCSchools
//
//  Created by Matthew Martindale on 6/22/21.
//

import Foundation

// Possible Errors API could encounter
enum APIError: Error {
    case invalidData
    case apiError(Error)
    case badResponse(Int)
    case noResponse
    case decodeError(Error)
    case badQuery
}

class APIController {
    
    // MARK: - Properties
    let validResponseCodes = Set.init(200...299)
    let urlSessionCaller: URLSessionCaller
    
    // MARK: - Initialization
    init(urlSessionCaller: URLSessionCaller = URLSession.shared) {
        self.urlSessionCaller = urlSessionCaller
    }
    
    // MARK: - API Calls
    // Fetch all NYC high schools
    func fetchSchools(completion: @escaping (Result<[School], APIError>) -> Void) {
        
        var urlComponents = URLComponents(string: APIConstants.baseSchoolsURLString)
        let apiTokenQuery = URLQueryItem(name: APIConstants.tokenKeyParam, value: APIToken.apiToken)
        urlComponents?.queryItems = [apiTokenQuery]
        
        guard let requestURL = urlComponents?.url else { return }
        var request = URLRequest(url: requestURL)
        request.httpMethod = "GET"
        
        // Use custom URLSession caller
        urlSessionCaller.fetch(with: request) { (possibleData, possibleResponse, possibleError) in
            if let error = possibleError {
                completion(.failure(.apiError(error)))
                return
            }
            
            guard let response = possibleResponse as? HTTPURLResponse else {
                completion(.failure(.noResponse))
                return
            }
            
            guard self.validResponseCodes.contains(response.statusCode) else {
                completion(.failure(.badResponse(response.statusCode)))
                return
            }
            
            guard let data = possibleData else {
                completion(.failure(.invalidData))
                return
            }
            
            let jsonDecoder = JSONDecoder()
            do {
                let schools = try jsonDecoder.decode([School].self, from: data)
                // Send back decoded School object in a Result type
                completion(.success(schools))
            } catch {
                completion(.failure(.decodeError(error)))
            }
        }
    }
    
    func fetchSAT(for school: School, completion: @escaping (Result<[SATScores], APIError>) -> Void) {
        var urlComponents = URLComponents(string: APIConstants.baseSATURLString)
        let apiTokenQuery = URLQueryItem(name: APIConstants.tokenKeyParam, value: APIToken.apiToken)
        let dbnQuery = URLQueryItem(name: APIConstants.dbnKeyParam, value: school.dbn)
        
        urlComponents?.queryItems = [apiTokenQuery, dbnQuery]
        guard let requestURL = urlComponents?.url else {
            completion(.failure(.badQuery))
            return
        }
        var request = URLRequest(url: requestURL)
        request.httpMethod = "GET"
        
        // Use custom URLSession caller
        urlSessionCaller.fetch(with: request) { (possibleData, possibleResponse, possibleError) in
            if let error = possibleError {
                completion(.failure(.apiError(error)))
                return
            }
            
            guard let response = possibleResponse as? HTTPURLResponse else {
                completion(.failure(.noResponse))
                return
            }
            
            guard self.validResponseCodes.contains(response.statusCode) else {
                completion(.failure(.badResponse(response.statusCode)))
                return
            }
            
            guard let data = possibleData else {
                completion(.failure(.invalidData))
                return
            }
            
            let jsonDecoder = JSONDecoder()
            do {
                let scores = try jsonDecoder.decode([SATScores].self, from: data)
                // Send back decoded SATScores object in a Result type
                completion(.success(scores))
            } catch {
                completion(.failure(.decodeError(error)))
                print(data)
            }
        }
        
    }
    
}
